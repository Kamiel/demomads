//
//  VSFirstViewController.h
//  DemoMADS
//
//  Created by Vim Solution on 6/13/14.
//  Copyright (c) 2014 Vim Solution. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MadsAdView;


@interface VSFirstViewController : UIViewController

@property (strong, nonatomic) MadsAdView *adView; // FIXME: looks like, it not work,
                                                  //        when load as IBOutlet from Storyboard...

- (IBAction)refresh:(id)sender;

@end
