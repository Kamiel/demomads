//
//  VSAppDelegate.h
//  DemoMADS
//
//  Created by Vim Solution on 6/13/14.
//  Copyright (c) 2014 Vim Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
