//
//  main.m
//  DemoMADS
//
//  Created by Vim Solution on 6/13/14.
//  Copyright (c) 2014 Vim Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VSAppDelegate class]));
    }
}
