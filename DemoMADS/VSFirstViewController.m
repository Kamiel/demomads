//
//  VSFirstViewController.m
//  DemoMADS
//
//  Created by Vim Solution on 6/13/14.
//  Copyright (c) 2014 Vim Solution. All rights reserved.
//

#import "VSFirstViewController.h"
#import <MadsSDK/MadsSDK.h>

static const NSString *zoneIdKey = @"zoneID";
static const NSString *secretKey = @"secret";

typedef NS_ENUM(NSUInteger, VSAdType) {
  VSAdTypeInline,
  VSAdTypeOverlay,
  VSAdTypeInterstial
};


@interface VSFirstViewController () <MadsAdViewDelegate, UIBarPositioningDelegate>

@property(readonly) NSDictionary *testScripts;
@property(assign, nonatomic) VSAdType adType;

@end

@implementation VSFirstViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  self.adView.delegate = self;
  self.adType = VSAdTypeInline;
  NSDictionary *script = self.testScripts[@(self.adType)];
  self.adView = [[MadsAdView alloc] initWithFrame:CGRectMake(0.f, 40.f, 200.f, 100.f)
                                             zone:script[zoneIdKey]
                                           secret:script[secretKey]
                                         delegate:self]; // ugly way to create view... =(
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  [self refresh:nil];
}

- (IBAction)refresh:(id)sender
{
  [self nextType];
  NSDictionary *script = self.testScripts[@(self.adType)];
  [self updateAdViewWithZoneID:script[zoneIdKey]
                        secret:script[secretKey]];
}

#pragma mark - private methods

- (NSDictionary *)testScripts
{
  return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ?
  @{@(VSAdTypeInline) : @{
        zoneIdKey : @"0337178053",
        secretKey : @"F0B4E489B0CFC0BB"
        },
    @(VSAdTypeOverlay) : @{
        zoneIdKey : @"8336743053",
        secretKey : @"BEF5D9D4D3E9B3CC"
        },
    @(VSAdTypeInterstial) : @{
        zoneIdKey : @"0336739057",
        secretKey : @"DA018F2094E8189C"
        }
    } :
  @{@(VSAdTypeInline) : @{
        zoneIdKey : @"3327876051",
        secretKey : @"5AC993C91380875B"
        },
    @(VSAdTypeOverlay) : @{
        zoneIdKey : @"0335449053",
        secretKey : @"9CD3FF68A812A11F"
        },
    @(VSAdTypeInterstial) : @{
        zoneIdKey : @"9335783055",
        secretKey : @"A487B22CA7A032DF"
        }
    };
}

- (void)nextType
{
  switch (self.adType) {
    case VSAdTypeInline:
    {
    self.adType = VSAdTypeOverlay;
    }
      break;
    case VSAdTypeOverlay:
    {
    self.adType = VSAdTypeInterstial;
    }
      break;
    case VSAdTypeInterstial:
    {
    self.adType = VSAdTypeInline;
    }
      break;
    default:
      break;
  }
}

- (void)updateAdViewWithZoneID:(NSString *)zoneID
                        secret:(NSString *)secret
{
  self.adView.zone = zoneID;
  self.adView.secret = secret;
  self.adView.madsAdType = self.adType == VSAdTypeInline ? MadsAdTypeInline : MadsAdTypeOverlay;
  self.adView.delegate = self;
  [self.adView update];
//  self.adView.frame = 
}

#pragma mark - MadsAdViewDelegate

// TODO: implement some methods here

@end
